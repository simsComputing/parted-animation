const path = require('path');

module.exports = {
    entry: './index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'parted-animation.js',
        library: 'partedAnimation',
        libraryTarget: 'umd'
    }
}