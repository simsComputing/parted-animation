/**
 * Class that represents part of an animation
 */
class AnimPart
{
    /**
     * The render callable takes one argument of type
     * AnimParams - It must return an object of type
     * AnimParams also
     * 
     * @param {number} duration 
     * @param {callable} render 
     */
    constructor(duration, render)
    {
        this.duration = duration;
        this.render = render;
        this.over = null;
        this.hasBegunAtTimestamp = null;
    }

    /**
     * Checks wether duration is exceeded or
     * else if the render callable has set
     * the over property to true
     * @param {number} timestamp 
     */
    isOver(timestamp) {
        if (null === this.hasBegunAtTimestamp) {
            return false;
        }

        if (this.over === true
            || this._isDurationExceeded(timestamp)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param {number} timestamp 
     */
    _isDurationExceeded(timestamp) {
        return this.duration 
        && this.hasBegunAtTimestamp + this.duration < timestamp;
    }

    /**
     * Needs to be called publicly before
     * the part is run
     * 
     * @param {number} timestamp 
     */
    begin(timestamp) {
        this.hasBegunAtTimestamp = timestamp;
        this.over = false;
    }

    /**
     * Runs the animation only if it is not over yet
     * 
     * @param {*} params 
     */
    run(params) {
        if (this.isOver(params.timestamp)) {
            return false;
        }

        if (this.hasBegunAtTimestamp === null) {
            this.begin(params.timestamp);
        }

        return this.render(params);
    }
}

export default AnimPart;