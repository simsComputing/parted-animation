# Parted Animation

This library is made to facilitate the construction of simple animations that need to be divided in many parts.

The main use case I had for this library was to handle smoothly an animation that needed to be first made frame by frame and then by pure javascript..

It allowed me to write two separate algorithms in order to decouple both logics and still have continuity in the animation.

The library allows all animation parts to communicate between themselves.

You can also create parts of the animation that will last for a specific time (like 1000ms) and have another part that will last for an undetermined time.

## How does it work ?

**1 - Load Images**

You need first to load the images you need for your animation.
There is no support yet for an animation without images but since the modifications to get there wouldn't be too hard to code, feel free to do it (or to ask politly).

Images are divided by sets of images.

Let's imagine i'll load a first set with 27 images and a second set with 48 images.

In each folders for set 1 and 2 images would have to be named 0.png / 1.png / 2.png ..... / x.png

```
var loader = new ImageLoader();
loader.addSet('setOne', '/path/to/set/one', 27, 'png');
loader.addSet('setTwo', '/path/to/set/two', 49, 'png');
```

**2 - Describe your animation**

In order to describe the animation you need to use the AnimPart class.
The constructor takes two arguments :
1 - The time it will last in ms. If the time is undetermined then you can set null.

2 - The callback used to display the animation. This callback takes one parameter that is an instance of the class AnimParams. The callback must return an object of the same class. The returned AnimParams will be passed to the next callback called for the animation (so it can be used to communicate between each frame of the animation but also between each AnimPart).


```
var animationDescription = [];

var initAnim = new AnimPart(1000, function (params) {
    // Make your animation here

    // Communicate with the following animPart :
    params.initAnim = 'I\'m done, your turn !!';

    return params;
});

var interactiveAnim = new AnimPart(null, function (params) {
    // make your animation here

    // Get some info from the previous animPart
    console.log(params.initAnim); // Print : I'm done, your turn !! 

    if (animationNeedsToEnd) {
        this.over;
        return;
    }
});

animationDescription.push(initAnim);
animationDescription.push(interactiveAnim);
```

In the second animation part, the fact of setting this.over to true will end
this part of the animation. This renderer won't be called anymore.

**3 - The information you can access inside an AnimPart**

When writing your callback for the anim part you can access multiple information.

The default params properties are :
    - params.timestamp : the timestamp passed to the callback of requestAnimationFrame (js native function).
    - params.canvas : the canvas inside which the animation takes place
    - params.imageLoader : the loader used to load your sets of images

For example to access the images for my setOne :
```
params.imageLoader.setOne
```

When inside your callback you also can access certain pieces of information through the 'this' keyword. 

    - this.duration : the duration of the animation part
    - this.render : the very callback you're in
    - this.over : boolean that indicates for the next frame weither or not this animation part is over
    - this.hasBegunAtTimestamp : the timestamp at which this part of the animation started

## Load and run the animation

Now that we have everything we need, we just have to do : 

```
myCanvas = document.getElementById('myCanvas');
var animation = new PartedAnimation(loader, animationDescription, myCanvas);
animation.run();
```

Note that the animation will run only after all images are loaded.
