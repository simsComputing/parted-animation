import AnimParams from './anim-params';

class PartedAnimation
{
    /**
     * 
     * @param {ImageLoader} loader 
     * @param {AnimPart[]} description 
     * @param {HTMLElement} canvas 
     */
    constructor(loader, description, canvas)
    {
        this.parts = description;
        this.loader = loader;
        this.context = canvas.getContext('2d');
        this.params = new AnimParams(
            0,
            canvas,
            loader,
        );

        this.currentAnimationPart = this.parts.shift();
    }

    /**
     * Load all images and run the animation
     */
    run() {
        this.loader._onLoaded = this._launchAnimation.bind(this);
        this.loader.load();
    }

    /**
     * Callback for the loader when animation is ready to go
     */
    _launchAnimation() {
        requestAnimationFrame(this._animate.bind(this));
    }

    /**
     * Renderer
     * 
     * @param {number} timestamp 
     */
    _animate(timestamp, newTurn = true) {
        if (newTurn === true) {
            this.params.previousTimestamp = this.params.timestamp;
            this.params.timestamp = timestamp;
        }

        var result = this.currentAnimationPart.run(this.params)

        if (result === false && this.parts.length > 0) {
            this.currentAnimationPart = this.parts.shift();
            this._animate(timestamp, false);
            return;
        } else if (result === false && this.parts.length == 0) {
            cancelAnimationFrame(this._animate.bind(this));
            return;
        }
        this.params = result;

        requestAnimationFrame(this._animate.bind(this));
    }
}

export default PartedAnimation;