class AnimParams
{
    constructor(
        timestamp,
        canvas,
        imageLoader
    ) {
        this.timestamp = timestamp;
        this.canvas = canvas;
        this.context = canvas.getContext('2d');
        this.imageLoader = imageLoader;
    }
}

export default AnimParams;