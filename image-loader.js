/**
 * Loads all images before the anim can be triggered
 */
class ImageLoader
{
    /**
     * 
     */
    constructor() {
        this.init = [];
        this.end = [];
        this.nbLoaded = 0;
        this.totalImages = 0;
        this.sets = [];
    }

    /**
     * Triggered when all images have finished loaded
     * Is supposed to be overwritten
     */
    _onLoaded() {
        console.log('Loaded !');
        console.log(this.nbLoaded);
    }

    /**
     * 
     * @param {string} name 
     * @param {string} path 
     * @param {number} nbImages 
     */
    addSet(name, path, nbImages, extension) {
        this.sets.push({
            name: name,
            path: path,
            nbImages,
            extension
        });
    }

    /**
     * Function that must be called publicly
     */
    load() {
        for (var index in this.sets) {
            var set = this.sets[index];
            this[set.name] = [];
            this.totalImages += set.nbImages;
        }

        for (var index in this.sets) {
            var set = this.sets[index];
            var path = set.path;
            for (var i = 0; i < set.nbImages; i++) {
                var image = this._loadImage(path + i.toString() + '.' + set.extension);
                this[set.name].push(image);                
            }            
        }
    }

    /**
     * Loads a single image
     */
    _loadImage(path) {
        var image = new Image();
        image.src = path;
        image.onload = this._imageLoaded.bind(this);
        return image;
    }

    /**
     * Triggered when an image has finished loaded
     */
    _imageLoaded() {
        this.nbLoaded++
        if (this.nbLoaded == this.totalImages) {
            this._onLoaded();
        }
    }
}

export default ImageLoader;